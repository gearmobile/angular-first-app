import { Component, OnInit, Input } from '@angular/core';
import { Recipe } from '../../../../contracts/recipe/recipe';

@Component({
  selector: 'ck-recipes-item',
  templateUrl: './recipes-item.component.html',
  styleUrls: ['./recipes-item.component.scss']
})
export class RecipesItemComponent implements OnInit {
  @Input() recipe: Recipe;

  constructor() {}

  ngOnInit() {}
}
