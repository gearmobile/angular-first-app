import { Component, OnInit } from '@angular/core';
import { Recipe } from '../../../contracts/recipe/recipe';

@Component({
  selector: 'ck-recipes-list',
  templateUrl: './recipes-list.component.html',
  styleUrls: ['./recipes-list.component.scss']
})
export class RecipesListComponent implements OnInit {
  recipes: Recipe[] = [];

  constructor() {
    this.recipes = [
      new Recipe(
        'Avocado Salsa',
        'Grilled Mahi Mahi With Avocado-Chile Salsa Recipe',
        'http://cdn-image.myrecipes.com/sites/default/files/image/recipes/hl/grilled-mahi-avocado-chile-salsa-xl.jpg'
      ),
      new Recipe(
        'Mexican Pinto',
        'Mexican Pinto Beans Recipe',
        'http://cdn-image.myrecipes.com/sites/default/files/image/recipes/sl/00/10/mexican-pinto-beans-mr-x.jpg'
      ),
      new Recipe(
        'Lemon-Basil Orzo',
        'Lemon-Basil Orzo with Parmesan Recipe',
        'http://cdn-image.myrecipes.com/sites/default/files/image/recipes/oh/wwar06/lemon-orzo-oh-x.jpg'
      )
    ];
  }

  ngOnInit() {}
}
