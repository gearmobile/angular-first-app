import { Component, OnInit } from '@angular/core';
import { Recipe } from '../../../contracts/recipe/recipe';

@Component({
  selector: 'ck-recipes-detail',
  templateUrl: './recipes-detail.component.html',
  styleUrls: ['./recipes-detail.component.scss']
})
export class RecipesDetailComponent implements OnInit {
  recipe: Recipe;

  constructor() {
    this.recipe = new Recipe(
      'Avocado Salsa',
      'Grilled Mahi Mahi With Avocado-Chile Salsa Recipe',
      'http://cdn-image.myrecipes.com/sites/default/files/image/recipes/hl/grilled-mahi-avocado-chile-salsa-xl.jpg'
    );
  }

  ngOnInit() {}
}
